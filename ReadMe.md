# Graphics Assignment 1  JetPack Joyride

###  Instructions for compiling 
--------------------------------------------------------------------------------
>- mkdir build
>- cd build
>- cmake ..
>- make all
>- ./graphics_asgn1

###  Instructions for playing
--------------------------------------------------------------------------------

Control:
>- Player can move leftward/rightward by pressing left/right key
>- Player can move upward with constant acceleration by pressing the space bar
>- Player can fire the water balloon by pressing the 'F' key
>- Player can escape the game by pressing the 'Esc' button

###  Zooming
--------------------------------------------------------------------------------
Scroll for Zooming in and out
