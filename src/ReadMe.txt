##Graphics Assignment 1  JetPack Joyride

###Instructions to compile the program
1 mkdir build
2 cd build
3 cmake ..
4 make all
5 ./graphics_asgn1

### Instructions to play the game

Control:
1 Player can move leftward/rightward by pressing left/right key
2 Player can move upward with constant acceleration by pressing the space bar
3 Player can fire the water balloon by pressing the 'F' key
4 Player can escape the game by pressing the 'Esc' button

##Zooming 
Scroll for Zooming in and out
