#include "special_objects.h"
#include "main.h"
#include <iostream>
using namespace std;
Special_objects::Special_objects(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    x_speed = 0.05;
    y_speed = 10.15;
    gravity = 9.8;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    static const GLfloat vertex_buffer_data1[] = {
        0.25f,0.25f,0.0f,
        0.125f,0.25f,0.0f,
        0.125f,0.0f,0.0f,
        0.125f,0.0f,0.0f,
        0.25f,0.0f,0.0f,
        0.25f,0.25f,0.0f,
        -0.25f,0.25f,0.0f,
        -0.125f,0.25f,0.0f,
        -0.125f,0.0f,0.0f,
        -0.125f,0.0f,0.0f,
        -0.25f,0.0f,0.0f,
        -0.25f,0.25f,0.0f
    };
    int n=100;    
    GLfloat vertex_buffer_data_m[(n/2)*9 + 36];
    for(int i=0;i<12*3;i++)
        vertex_buffer_data_m[i] = vertex_buffer_data1[i];
    int temp=36;
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = -2*PI/n;
    double x0=0.25f,y0=0.0f,z0=0.0f;
    double x1=0.0f,y1=0.0f;
    for(int i=0;i<n/2;i++)
    {
        vertex_buffer_data_m[temp++] = 0.0f;
        vertex_buffer_data_m[temp++] = 0.0f; 
        vertex_buffer_data_m[temp++] = 0.0f;
        vertex_buffer_data_m[temp++] = x0;
        vertex_buffer_data_m[temp++] = y0; 
        vertex_buffer_data_m[temp++] = z0; 
        x1 = x0*cos(arg)-y0*sin(arg);
        y1 = x0*sin(arg)+y0*cos(arg);
        x0 = x1;
        y0 = y1;
        vertex_buffer_data_m[temp++] = x0;
        vertex_buffer_data_m[temp++] = y0;
        vertex_buffer_data_m[temp++] = z0;
    }

    // HEART
    n=20;    
    GLfloat vertex_buffer_data_h[n*9 + 9];
    temp=0;
    arg = 2*PI/n;
    x0=0.125f,y0=0.0f,z0=0.0f;
    x1=0.0f,y1=0.0f;
    temp=0;
    for(int i=0;i<n/2;i++)
    {
        vertex_buffer_data_h[temp++] = -0.25f;
        vertex_buffer_data_h[temp++] = 0.0f; 
        vertex_buffer_data_h[temp++] = 0.0f;
        vertex_buffer_data_h[temp++] = x0-0.25f;
        vertex_buffer_data_h[temp++] = y0; 
        vertex_buffer_data_h[temp++] = z0; 
        x1 = x0*cos(arg)-y0*sin(arg);
        y1 = x0*sin(arg)+y0*cos(arg);
        x0 = x1;
        y0 = y1;
        vertex_buffer_data_h[temp++] = x0-0.25f;
        vertex_buffer_data_h[temp++] = y0;
        vertex_buffer_data_h[temp++] = z0;
    }    
    x0=0.125f,y0=0.0f,z0=0.0f;
    x1=0.0f,y1=0.0f;
    for(int i=0;i<n/2;i++)
    {
        vertex_buffer_data_h[temp++] = 0.0f;
        vertex_buffer_data_h[temp++] = 0.0f; 
        vertex_buffer_data_h[temp++] = 0.0f;
        vertex_buffer_data_h[temp++] = x0;
        vertex_buffer_data_h[temp++] = y0; 
        vertex_buffer_data_h[temp++] = z0; 
        x1 = x0*cos(arg)-y0*sin(arg);
        y1 = x0*sin(arg)+y0*cos(arg);
        x0 = x1;
        y0 = y1;
        vertex_buffer_data_h[temp++] = x0;
        vertex_buffer_data_h[temp++] = y0;
        vertex_buffer_data_h[temp++] = z0;
    }
    vertex_buffer_data_h[temp++] = 0.125f;
    vertex_buffer_data_h[temp++] = 0.0f;
    vertex_buffer_data_h[temp++] = 0.0f;
    vertex_buffer_data_h[temp++] = -0.375f;
    vertex_buffer_data_h[temp++] = 0.0f;
    vertex_buffer_data_h[temp++] = 0.0f;
    vertex_buffer_data_h[temp++] = -0.125f;
    vertex_buffer_data_h[temp++] = -0.25f;
    vertex_buffer_data_h[temp++] = 0.0f;

    //SHEILD
    GLfloat vertex_buffer_data_s[] = {
        -0.25f,0.0f,0.0f,
         0.25f,0.0f,0.0f,
         0.0f,-0.25f,0.0f,
         -0.25f,0.0f,0.0f,
         0.0f,0.0f,0.0f,
         -0.125f,0.125f,0.0f,
         0.25f,0.0f,0.0f,
         0.0f,0.0f,0.0f,
         0.125f,0.125f,0.0f,
         0.0f,0.0f,0.0f,
         0.125f,0.125f,0.0f,
         -0.125f,0.125f,0.0f
    };


    this->object_m = create3DObject(GL_TRIANGLES, (100/2+4)*3, vertex_buffer_data_m, color, GL_FILL);
    this->object_h = create3DObject(GL_TRIANGLES, (n+1)*3, vertex_buffer_data_h, color, GL_FILL);
    this->object_s = create3DObject(GL_TRIANGLES, (4)*3, vertex_buffer_data_s, color, GL_FILL);
}

void Special_objects::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(0.1f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    glm::mat4 scale1 = glm::scale (scale);    
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (scale1 * translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    // draw3DObject(this->object);
   if(this->flag == 0)//magnet
        draw3DObject(this->object_m);
    if(this->flag == 1)//heart
        draw3DObject(this->object_h);
    if(this->flag == 2)//sheild
        draw3DObject(this->object_s);
}

void Special_objects::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Special_objects::tick() {
    double inter = cur() - prev();
    // cout << inter << endl;
    double temp = (y_speed*inter - (gravity)*inter*inter/2);
    y_speed -= gravity*inter;

    this->position.x -=x_speed;
    if(this->position.y + temp >= -5.0f && this->position.y + temp <= 5.0f)
       this->position.y+=temp;

    if(this->position.y < -4.0f)
        y_speed = 10.15;
    if(this->position.y > 4.0f)
        y_speed = -10.15;

    if(this->position.x < -14.0f)
        this->exist = false;
}   


