#include "main.h"
#include "special_objects.h"
#include "timer.h"
#include "ball.h"
#include "dragon.h"
#include "ring.h"
#include "print_text.h"
#include "boomerang.h"
#include "enemy1.h"
#include "coin.h"
#include "water_ballon.h"
#include "platform.h"
#include <iostream>
using namespace std;

GLMatrices Matrices;
GLuint     programID;
GLFWwindow *window;

/**************************
* Customizable functions *
**************************/

Ball ball2;
Boomerang b1;
Ring r1;
Dragon tryout;
int df;
vector<Coin> coins;
vector<Water_ballon> wb;
vector<Water_ballon> fb;
vector<Coin> ice_ball;
vector<Print_text> score_print;
vector<Print_text> lives_print;
vector<Print_text> letter_print;
vector<Special_objects> sobj;
vector<Platform> platforms;
vector<Platform> ceilings;
vector<Enemy1> enemies1;
vector<Enemy1> enemies2;
double score_pos = 0.0f;
double lives_pos = 0.0f;
double letter_pos = 0.0f;
double cur_time=0;
double prev_time=0;
int level_change=10;
double PI = 3.141592653589793238462643383279502884197169399375105820974944;

float screen_zoom = 1, screen_center_x = 0, screen_center_y = 0;
float camera_rotation_angle = 0;

Timer t60(1.0 / 20);
Timer t30(13.0 / 1.0);
Timer t30a(30.0 / 1.0);
Timer t5(5.0 / 1.0);
Timer t5a(5.0 / 1.0);
Timer t4(2.0 / 1.0);
Timer t5b(5.0 / 1.0);
/* Render the scene with openGL */
/* Edit this function according to your assignment */
void draw() {
    // clear the color and depth in the frame buffer
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // use the loaded shader program
    // Don't change unless you know what you are doing
    glUseProgram (programID);

    // Eye - Location of camera. Don't change unless you are sure!!
    glm::vec3 eye ( 0, 0, 5 );
    // glm::vec3 eye ( 5*cos(camera_rotation_angle*M_PI/180.0f), 0, 5*sin(camera_rotation_angle*M_PI/180.0f) );
    // Target - Where is the camera looking at.  Don't change unless you are sure!!
    glm::vec3 target (0, 0, 0);
    // Up - Up vector defines tilt of camera.  Don't change unless you are sure!!
    glm::vec3 up (0, 1, 0);

    // Compute Camera matrix (view)
    Matrices.view = glm::lookAt( eye, target, up ); // Rotating Camera for 3D
    // Don't change unless you are sure!!
    // Matrices.view = glm::lookAt(glm::vec3(0, 0, 3), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0)); // Fixed camera for 2D (ortho) in XY plane

    // Compute ViewProject matrix as view/camera might not be changed for this frame (basic scenario)
    // Don't change unless you are sure!!
    glm::mat4 VP = Matrices.projection * Matrices.view;

    // Send our transformation to the currently bound shader, in the "MVP" uniform
    // For each model you render, since the MVP will be different (at least the M part)
    // Don't change unless you are sure!!
    glm::mat4 MVP;  // MVP = Projection * View * Model
    glm::vec3 scale = glm::vec3(1.0f,1.0f,0.0f);
    // Scene render

    scale = glm::vec3(8.90f,0.5f,0.0f);
    vector<Platform>::iterator it1;
    it1 = platforms.begin();
    int turn=1;
    while(it1!=platforms.end())
    {
        if(!(*it1).exist)
        {
            platforms.erase(it1);
            continue;
        }
        if(turn = 1)
        {
            turn = 0;
            scale = glm::vec3(4.0f,0.5f,0.0f);
        }
        else
        {
            turn =1;
            scale = glm::vec3(4.0f,0.5f,0.0f);
        }
        (*it1).draw(VP,scale);
        it1++;
    }
    turn=1;
    it1 = ceilings.begin();
    while(it1!=ceilings.end())
    {
        if(!(*it1).exist)
        {
            ceilings.erase(it1);
            continue;
        }
        if(turn = 1)
        {
            turn = 0;
            scale = glm::vec3(4.0f,0.5f,0.0f);
        }
        else
        {
            turn =1;
            scale = glm::vec3(4.0f,0.5f,0.0f);
        }
        (*it1).draw(VP,scale);
        it1++;
    }
    scale = glm::vec3(1.0f,1.0f,0.0f);
    //Semi-circular Ring
    if(r1.exist)
        r1.draw(VP,scale);
    //Player
    ball2.draw(VP,scale);
    if(tryout.exist)
        tryout.draw(VP,scale);
    //Ice balls
    vector<Coin>::iterator ic_it;
    ic_it = ice_ball.begin();
    while(ic_it!=ice_ball.end())
    {
        if(!ic_it->exist)
        {
            ice_ball.erase(ic_it);
            continue;
        }
        ic_it->draw(VP,scale);
        ic_it++;
    }
    scale = glm::vec3(0.125f,0.125f,0.0f);

    //Display score lives and levels
    vector<Print_text>:: iterator s_it;
    s_it = score_print.begin();
    while(s_it!=score_print.end())
    {
        s_it->draw(VP,scale);
        s_it++;
    }
    s_it = lives_print.begin();
    while(s_it!=lives_print.end())
    {
        s_it->draw(VP,scale);
        s_it++;
    }
    s_it = letter_print.begin();
    while(s_it!=letter_print.end())
    {
        s_it->draw(VP,scale);
        s_it++;
    }

    scale = glm::vec3(1.0f,1.0f,0.0f);
    //Boomerang 
    if(b1.exist)
        b1.draw(VP,scale);
    //Fire beams and fire lines
    vector<Enemy1>:: iterator eit;
    eit = enemies1.begin();
    while(eit!=enemies1.end())
    {
        if(!eit->exist)
        {
            enemies1.erase(eit);
            continue;
        }
        (*eit).draw(VP,scale);
        (eit->c1).draw(VP,scale);
        (eit->c2).draw(VP,scale);
        eit++;
    }
    eit = enemies2.begin(); 
    while(eit!=enemies2.end())
    {
        if(!eit->exist)
        {
            enemies2.erase(eit);
            continue;
        }
        (*eit).draw(VP,scale);
        (eit->c1).draw(VP,scale);
        (eit->c2).draw(VP,scale);
        eit++;
    }
    //Water Ballons
    vector<Water_ballon>::iterator w_it;
    w_it = wb.begin();
    while(w_it!=wb.end())
    {
        if(!w_it->exist)
        {
            wb.erase(w_it);
            continue;
        }
        w_it->draw(VP,scale);
        w_it++;
    }
    //Jet Propulsion
    scale = glm::vec3(0.5f,0.5f,0.0f);
    w_it = fb.begin();
    while(w_it!=fb.end())
    {
        if(!w_it->exist)
        {
            fb.erase(w_it);
            continue;
        }
        w_it->draw(VP,scale);
        w_it++;
    }

    scale = glm::vec3(1.0f,1.0f,0.0f);
    //Magnets and special objects such as heart and shield
    vector<Special_objects>::iterator sobjit;
    sobjit = sobj.begin();
    while(sobjit!=sobj.end())
    {
        if(!sobjit->exist && sobjit->flag!=0)
        {
            sobj.erase(sobjit);
            continue;
        }
        (*sobjit).draw(VP,scale);
        sobjit++;
    }
    scale = glm::vec3(1.0f,1.0f,0.0f);
    //Simple coins
    vector<Coin> :: iterator it;
    it = coins.begin();
    while(it!=coins.end())
    {
        if(!(*it).exist)
        {
            coins.erase(it);
            continue;
        }
        (*it).draw(VP,scale);
        it++;
    } 
}
void tick_input(GLFWwindow *window) {
    int left  = glfwGetKey(window, GLFW_KEY_LEFT);
    int right = glfwGetKey(window, GLFW_KEY_RIGHT);
    int up = glfwGetKey(window, GLFW_KEY_SPACE);
    int fire = glfwGetKey(window, GLFW_KEY_F);
    if(left)
        ball2.tick(0);
    if(right)
        ball2.tick(1);
    if(up)
    {
        double x = rand()%100+1;
        Water_ballon temp = Water_ballon(2*ball2.position.x-x/100,2*ball2.position.y-1.0f,COLOR_ORANGE,1);
        if(ball2.y_speed<0)
            temp.y_speed = 4*ball2.y_speed;
        else
            temp.y_speed = -4*ball2.y_speed;
        temp.x_speed = 0;
        fb.push_back(temp);
        ball2.tick(2);
    }
    else
        ball2.tick(3);
    if(fire)
    {
        Water_ballon temp = Water_ballon(ball2.position.x,ball2.position.y,COLOR_LIGHTBLUE,0);
        wb.push_back(temp);
    }
}
double cur()
{
    return cur_time;
}
double prev()
{
    return prev_time;
}
void tick_elements() {
    camera_rotation_angle += 1;
}
/* Initialize the OpenGL rendering properties */
/* Add all the models to be created here */
void initGL(GLFWwindow *window, int width, int height) {
    /* Objects should be created before any other gl function and shaders */
    // Create the models
    ball2 = Ball(-2.5, 0.5, COLOR_GREEN);
    r1 = Ring(-2.5, 0.5, COLOR_GREEN);
    b1 = Boomerang(-2.5, 0.5, COLOR_GREEN);
    r1.exist = false;
    b1.exist = false;   
    int dx = rand()%400+200;
    tryout = Dragon(dx/100,0.0f,COLOR_ORANGE);
    tryout.exist = false;
    df=1;
    programID = LoadShaders("Sample_GL.vert", "Sample_GL.frag");
    // Get a handle for our "MVP" uniform
    Matrices.MatrixID = glGetUniformLocation(programID, "MVP");
    reshapeWindow (window, width, height);
    // Background color of the scene
    glClearColor (COLOR_BACKGROUND.r / 256.0, COLOR_BACKGROUND.g / 256.0, COLOR_BACKGROUND.b / 256.0, 0.0f); // R, G, B, A
    glClearDepth (1.0f);
    glEnable (GL_DEPTH_TEST);
    glDepthFunc (GL_LEQUAL);
    cout << "VENDOR: " << glGetString(GL_VENDOR) << endl;
    cout << "RENDERER: " << glGetString(GL_RENDERER) << endl;
    cout << "VERSION: " << glGetString(GL_VERSION) << endl;
    cout << "GLSL: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
}
void collision_special_objects()
{
    vector<Special_objects>::iterator it;
    it = sobj.begin();
    while(it!=sobj.end())
    {
        if(abs(ball2.position.x - (*it).position.x) < 0.75f && abs(ball2.position.y - (*it).position.y) < 0.75f)
        {
            (*it).exist = false;
            if(it->flag == 0 )
            {
                t4.prev = glfwGetTime();
                ball2.x_acc = 3.0f;
            }
            if(it->flag == 1)
                ball2.lives++;
            if(it->flag == 2)
                ball2.sheild++;
        }
        it++;
    }
}
void collision_ring()
{
    if(!r1.exist)
        return;
    if(r1.theta <= 2*PI && ((r1.position.x - ball2.position.x)*(r1.position.x - ball2.position.x) + (r1.position.y - ball2.position.y)*(r1.position.y - ball2.position.y)) <= 6.0f  && r1.position.y >= ball2.position.y)
        ball2.ring = 1;
    else
    {
        ball2.ring = 0;
        r1.theta = PI;
    }
    if(ball2.ring == 1)
    {
        ball2.position.x = r1.position.x + (1.0f)*(cos(r1.theta));
        ball2.position.y = r1.position.y + (1.0f)*(sin(r1.theta));
        r1.theta += 5*(PI/180);
        if(r1.theta > 2*PI)
            r1.exist = false;
    }
}
void collision_water()
{
    vector<Enemy1>::iterator it;
    it = enemies1.begin();
    vector<Water_ballon>::iterator w;
    while(it!=enemies1.end())
    {
        double slope = (it->pt1_y - it->pt2_y)/(it->pt1_x - it->pt2_x);
        w = wb.begin();
        while(w!=wb.end())
        {
            if((abs(w->position.x - (it->c1).position.x) < 0.75f && abs(w->position.y - (it->c1).position.y) < 0.75f) || (abs(w->position.x - (it->c2).position.x) < 0.75f && abs(w->position.y - (it->c2).position.y) < 0.75f))
            {
                it->exist = false;
                w->exist =false;
            }
            double temp = (w->position.y - it->pt1_y)/(w->position.x - it->pt1_x);
            if(abs(abs(temp) - abs(slope))<=5 && w->position.x >= min(it->pt1_x , it->pt2_x) && w->position.x <= max(it->pt1_x ,it->pt2_x) && w->position.y >= min(it->pt1_y , it->pt2_y) && w->position.y <= max(it->pt1_y , it->pt2_y)) 
            {
                    it->exist = false;
                    w->exist = false;
            }
            w++;
        }
        it++;
    }
    it = enemies2.begin();
    while(it!=enemies2.end())
    {
        double slope = (it->pt1_y - it->pt2_y)/(it->pt1_x - it->pt2_x);
        w = wb.begin();
        while(w!=wb.end())
        {
            if((abs(w->position.x - (it->c1).position.x) < 0.75f && abs(w->position.y - (it->c1).position.y) < 0.75f) || (abs(w->position.x - (it->c2).position.x) < 0.75f && abs(w->position.y - (it->c2).position.y) < 0.75f))
            {
                it->exist = false;
                w->exist =false;
            }
            double temp = (w->position.y - it->pt1_y)/(w->position.x - it->pt1_x);
            if(abs(abs(temp) - abs(slope))<=5 && w->position.x >= min(it->pt1_x , it->pt2_x) && w->position.x <= max(it->pt1_x ,it->pt2_x) && abs(w->position.y - it->pt1_y)<=1 ) 
            {
                    it->exist = false;
                    w->exist = false;
            }
            w++;
        }
        it++;
    }
}
void collision_boomerang()
{
    if(abs(ball2.position.x - (b1).position.x) < 0.75f && abs(ball2.position.y - (b1).position.y) < 0.75f && b1.exist==true)
    {
        (b1).exist = false;
        if(ball2.ring!=1)
        {
            if(ball2.sheild==0)
                ball2.lives--;
            else
                ball2.sheild--;
        }
    }
}    
void collision_coin()
{
    vector<Coin>::iterator it;
    it = coins.begin();
    while(it!=coins.end())
    {
        if(abs(ball2.position.x - (*it).position.x) < 0.75f && abs(ball2.position.y - (*it).position.y) < 0.75f)
        {
            (*it).exist = false;
            if(it->type==0)
                ball2.score+=2;
            else
                ball2.score++;
        }
        it++;
    }
    it = ice_ball.begin();
    while(it!=ice_ball.end())
    {
        if(abs(ball2.position.x - (*it).position.x) < 0.75f && abs(ball2.position.y - (*it).position.y) < 0.75f)
        {
            (*it).exist = false;
                ball2.lives--;
                ball2.position.y = -3.0f;
        }
        it++;
    }
}
void collision_enemy1()
{
    vector<Enemy1>::iterator eit;
    eit = enemies1.begin();
    while(eit!=enemies1.end())
    {
        double slope = (eit->pt1_y - eit->pt2_y)/(eit->pt1_x - eit->pt2_x);
        double temp = (ball2.position.y - eit->pt1_y)/(ball2.position.x - eit->pt1_x);
        if((abs(ball2.position.x - (eit->c1).position.x) < 0.75f && abs(ball2.position.y - (eit->c1).position.y) < 0.75f) || (abs(ball2.position.x - (eit->c2).position.x) < 0.75f && abs(ball2.position.y - (eit->c2).position.y) < 0.75f))
        {
            eit->exist = false;
            if(ball2.ring!=1)
            {
                if(ball2.sheild == 0)
                    ball2.lives--;
                else
                    ball2.sheild--;
                ball2.position.y = -3.0f;
            }
        }
        if(abs(abs(temp) - abs(slope))<=5 && ball2.position.x >= min(eit->pt1_x , eit->pt2_x) && ball2.position.x <= max(eit->pt1_x , eit->pt2_x) && ball2.position.y >= min(eit->pt1_y , eit->pt2_y) && ball2.position.y <= max(eit->pt1_y , eit->pt2_y)) 
        {
            if(ball2.ring!=1)
            {
                eit->exist = false;
                if(ball2.sheild == 0)
                    ball2.lives--;
                else
                    ball2.sheild--;
                ball2.position.y = -3.0f;
            }
        }
        eit++;
    }
    eit = enemies2.begin();
    while(eit!=enemies2.end())
    {
        double slope = (eit->pt1_y - eit->pt2_y)/(eit->pt1_x - eit->pt2_x);
        double temp = (ball2.position.y - eit->pt1_y)/(ball2.position.x - eit->pt1_x);
        if((abs(ball2.position.x - (eit->c1).position.x) < 0.75f && abs(ball2.position.y - (eit->c1).position.y) < 0.75f) || (abs(ball2.position.x - (eit->c2).position.x) < 0.75f && abs(ball2.position.y - (eit->c2).position.y) < 0.75f))
        {
            eit->exist = false;
            if(ball2.ring!=1)
            {
                if(ball2.sheild == 0)
                    ball2.lives--;
                else
                    ball2.sheild--;
                ball2.position.y = -3.0f;
            }
        }
        if(abs(abs(temp) - abs(slope))<=5 && ball2.position.x >= min(eit->pt1_x , eit->pt2_x) && ball2.position.x <= max(eit->pt1_x , eit->pt2_x) && ball2.position.y >= min(eit->pt1_y , eit->pt2_y) && ball2.position.y <= max(eit->pt1_y , eit->pt2_y)) 
        {
            if(ball2.ring!=1)
            {
                eit->exist = false;
                if(ball2.sheild == 0)
                    ball2.lives--;
                else
                    ball2.sheild--;
                ball2.position.y = -3.0f;
            }
        }
        eit++;
    }
}
Print_text fix_pos(int x,Print_text temp)
{
    temp.L_1=0,temp.L_2=0,temp.L_3=0,temp.L_4=0,temp.L_5=0,temp.L_6=0,temp.L_7=0,temp.L_8=0,temp.L_9=0,temp.L_10=0,temp.L_11=0;
    if(x==0)
        temp.L_1 =1,temp.L_3 =1,temp.L_4 =1,temp.L_5 =1,temp.L_6 =1,temp.L_7 =1;
    if(x==1)
        temp.L_6 =1,temp.L_7 =1;
    if(x==2)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_5 =1,temp.L_6 =1;
    if(x==3)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_6 =1,temp.L_7 =1;
    if(x==4)
        temp.L_2 =1,temp.L_4 =1,temp.L_6 =1,temp.L_7 =1;
    if(x==5)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_4 =1,temp.L_7 =1;
    if(x==6)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_4 =1,temp.L_5 =1,temp.L_7 =1;
    if(x==7)
        temp.L_1 =1,temp.L_6 =1,temp.L_7 =1;
    if(x==8)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_4 =1,temp.L_5 =1,temp.L_6 =1,temp.L_7 =1;
    if(x==9)
        temp.L_1 =1,temp.L_2 =1,temp.L_3 =1,temp.L_4 =1,temp.L_6 =1,temp.L_7 =1;   
    return temp;
}
void print_score()
{
    score_print.clear();
    int temp = 1000;
    while(temp>1 && ball2.score%temp != 0 && ball2.score/temp == 0)
        temp = temp/10;
    if(ball2.score == 0)
        temp =10;
    score_pos=0.0f;
    int x = (ball2.score/temp);
    while(temp!=0)
    {
        Print_text t = Print_text(-20.0f+score_pos,23.0f,COLOR_BLACK);
        score_pos+=1.5f;
        t = fix_pos(x,t);
        t.number = x;
        x = ball2.score%temp;
        temp = temp/10;
        score_print.push_back(t);
    }
    return ;
}
void print_lives()
{
    if(ball2.lives < 0)
        return ;
    lives_print.clear();
    int temp = 1000;
    while(temp>1 && ball2.lives%temp != 0 && ball2.lives/temp == 0)
        temp = temp/10;
    if(ball2.lives == 0)
        temp =10;
    lives_pos=0.0f;
    int x = (ball2.lives/temp);
    while(temp!=0)
    {
        Print_text t = Print_text(-20.0f+lives_pos,20.0f,COLOR_BLACK);
        lives_pos+=1.5f;
        t = fix_pos(x,t);
        t.number = x;
        x = ball2.lives%temp;
        temp = temp/10;
        lives_print.push_back(t);
    }
    return ;
}
void print_levels()
{
    int x = ball2.level;
    Print_text t = Print_text(-20.0f,16.0f,COLOR_BLACK);
    t = fix_pos(x,t);
    t.number = x;
    lives_print.push_back(t);
    return ;
}
Print_text fix_letter(char t,Print_text temp)
{
    temp.L_1=0,temp.L_2=0,temp.L_3=0,temp.L_4=0,temp.L_5=0,temp.L_6=0,temp.L_7=0,temp.L_8=0,temp.L_9=0,temp.L_10=0,temp.L_11=0;
    if(t=='S')
        temp.L_1=1,temp.L_2=1,temp.L_3=1,temp.L_4=1,temp.L_7=1;
    if(t=='O')
        temp.L_1=1,temp.L_3=1,temp.L_4=1,temp.L_5=1,temp.L_6=1,temp.L_7=1;
    if(t=='C')
        temp.L_1=1,temp.L_3=1,temp.L_4=1,temp.L_5=1;        
    if(t=='R')
        temp.L_1=1,temp.L_2=1,temp.L_4=1,temp.L_5=1,temp.L_6=1,temp.L_9=1;        
    if(t=='E')
        temp.L_1=1,temp.L_2=1,temp.L_3=1,temp.L_4=1,temp.L_5=1;        
    if(t=='L')
        temp.L_4=1,temp.L_5=1,temp.L_3=1;
    if(t=='I')
        temp.L_6=1,temp.L_7=1;
    if(t=='V')
        temp.L_4=1,temp.L_6=1,temp.L_8=1,temp.L_10=1;
    return temp;

}
void print_letters()
{
    string t;
    letter_print.clear();

    t = "SCORE";
    int len = t.length();
    letter_pos=0.0f;
    int i=0;
    while(i<len)
    {
        Print_text temp;
        temp = Print_text(-28.0f+letter_pos,23.0f,COLOR_BLACK);
        letter_pos+=1.5f;
        temp = fix_letter(t[i],temp);
        letter_print.push_back(temp);   
        i++;
    }
    t = "LIVES";
    len = t.length();
    letter_pos=0.0f;
    i=0;
    while(i<len)
    {
        Print_text temp;
        temp = Print_text(-28.0f+letter_pos,20.0f,COLOR_BLACK);
        letter_pos+=1.5f;
        temp = fix_letter(t[i],temp);
        letter_print.push_back(temp);
        i++;   
    }
}
int main(int argc, char **argv) {
    srand(time(0));
    int width  = width_screen;
    int height = height_screen;
    window = initGLFW(width, height);
    initGL (window, width, height);
    /* Draw in loop */
    int turn=1,turn1=1;
    double pos = -3.0f,pos1 = -3.0f;
    while (!glfwWindowShouldClose(window)) {
        // Process timers
        if (t60.processTick()) {
            cur_time = glfwGetTime();
            // 60 fps
            // OpenGL Draw commands
            if(ball2.lives < 0)
            {
                quit(window);
                return 0;
            }
            if(ball2.level > 1 && !tryout.exist && df==0)
            {
                int dx = rand()%400;
                tryout = Dragon(dx/100,0.0f,COLOR_ORANGE);
                df=1;
            }
            if(t5b.processTick())
            {
                if(tryout.exist == false)
                    df=0;
                tryout.exist = false;
            }
            if(wb.size() > 10)
                wb.clear();
            if(fb.size() > 10)
                fb.clear();
            if(ball2.score > level_change)
            {
                ball2.level++;
                level_change = 2*level_change;
            }
            if(tryout.exist && ball2.position.y < tryout.position.y)
                tryout.tick(3);
            if(tryout.exist && ball2.position.y > tryout.position.y)
                tryout.tick(2);
            if(tryout.exist && abs(tryout.position.x - ball2.position.x)<1.0f)
            {
                if(tryout.position.x > ball2.position.x)
                    tryout.tick(1);
                else
                    tryout.tick(0);
            }
            if(tryout.exist)
            {
                if(tryout.position.x > ball2.position.x)
                    tryout.type = 0;
                else
                    tryout.type = 1;
            }
            if(tryout.exist && ice_ball.size()<10 && abs(ball2.position.y - tryout.position.y) <0.5f )
            {
                Coin temp = Coin(tryout.position.x,tryout.position.y,COLOR_WGREY,4);
                if(tryout.position.x > ball2.position.x)
                    temp.x_speed = -0.5;
                else
                    temp.x_speed = 0.5f;
                tryout.c1 = temp;
                ice_ball.push_back(temp);
            }

            if(ball2.position.x>3.0f)//Panning shifting all objects to the left if the player comes to the extreme right
            {
                ball2.position.x-=4.0f;
                if(b1.exist)
                    b1.position.x-=4.0f;
                if(r1.exist)
                    r1.position.x-=4.0f;
                vector<Coin>::iterator c_it;
                c_it = coins.begin();
                while(c_it!=coins.end())
                {
                    c_it->position.x-=4.0f;
                    c_it++;
                }
                vector<Water_ballon>::iterator w_it;
                w_it = wb.begin();
                while(w_it!=wb.end())
                {
                    w_it->position.x-=4.0f;
                    w_it++;
                }
                w_it = fb.begin();
                while(w_it!=fb.end())
                {
                    w_it->position.x-=4.0f;
                    w_it++;
                }
                vector<Special_objects>::iterator sobj_it;
                sobj_it = sobj.begin();
                while(sobj_it!=sobj.end())
                {
                    sobj_it->position.x-=4.0f;
                    sobj_it++;
                }
                vector<Enemy1>::iterator e_it;
                e_it = enemies1.begin();
                while(e_it!=enemies1.end())
                {
                    e_it->position.x-=4.0f,e_it->c1.position.x -=4.0f,e_it->c2.position.x -=4.0f;
                    e_it++;
                }
                e_it = enemies2.begin();
                while(e_it!=enemies2.end())
                {
                    e_it->position.x-=4.0f,e_it->c1.position.x -=4.0f,e_it->c2.position.x -=4.0f;
                    e_it++;
                }
            }
            if(!b1.exist && t30.processTick()) // Boomerang occurs every 13 sec
            {
                double bx = rand()%40+50;
                bx = bx/10;
                double by = sqrt((bx+4)/(rand()%2+1));
                b1 = Boomerang(bx,by,COLOR_GREEN);
            }
            if(!r1.exist && t30a.processTick() && ball2.level>2)//Rings comes after every 30 sec and after level 2
            {
                int bx = rand()%400+500;
                int by = rand()%300;
                r1 = Ring(bx/100,by/100,COLOR_GOLD);
                r1.exist = true;
            }
            print_score();
            print_lives();
            print_levels();
            print_letters();
            if(sobj.size()<2 && ((int)cur_time)%20==0 && cur_time>20)//special objects after 20 sec and 2 at a time
            {
                int rnd_x = rand() % 800 + 800;
                int rnd_y = rand() % 400;
                int sign_y = rand() %3;
                Special_objects temp;
                if(sign_y == 0)
                    temp = Special_objects(rnd_x/100,rnd_y/100,COLOR_RED);
                if(sign_y == 1)
                    temp = Special_objects(rnd_x/100,rnd_y/100,COLOR_RED);
                if(sign_y == 2)
                    temp = Special_objects(rnd_x/100,rnd_y/100,COLOR_BLUE);
                temp.flag = sign_y;
                temp.exist = true;
                sobj.push_back(temp);
            }
            while(coins.size()<5)//Coins 5 at a time
            {
                int rnd_x = rand() % 250 + 700;
                int rnd_y = rand() % 400;
                int sign_y = rand() %2;
                Coin temp;
                int x = rand()%4;
                if(sign_y==1)
                    temp = Coin(rnd_x/100,rnd_y/100,COLOR_GOLD,x);
                else
                    temp = Coin(rnd_x/100,-rnd_y/100,COLOR_GOLD,x);
                coins.push_back(temp);
            }
            if(enemies1.size()<2 && t5.processTick())//Enemies 2 at a time
            {
                double pt1_x,pt1_y,pt2_x,pt2_y;
                pt1_x = rand() %575 +200;
                pt1_x =pt1_x/100;
                pt1_y = (rand() %300);
                pt1_y = pt1_y/100;
                pt2_x = pt1_x+3;
                while(abs(pt2_x-pt1_x)>2)
                {
                    pt2_x = (rand() %575 +200);
                    pt2_x = pt2_x/100;
                }
                pt2_y = pt1_y + 3;
                while(abs(pt2_y-pt1_y)>2)
                {
                    pt2_y = (rand() %300);
                    pt2_y = pt2_y/100;
                }
                double l = sqrt((pt2_x-pt1_x)*(pt2_x-pt1_x) + (pt2_y-pt1_y)*(pt2_y-pt1_y))/2;
                Enemy1 temp;
                if(pt1_x == pt2_x)
                    temp = Enemy1((pt1_x+pt2_x)/2,(pt2_y+pt1_y)/2,COLOR_RED,PI/2,l/cos(2*PI/180));
                else
                    temp = Enemy1((pt1_x+pt2_x)/2,(pt2_y+pt1_y)/2,COLOR_RED,atan((pt2_y - pt1_y)/(pt2_x - pt1_x)),l/cos(2*PI/180));
                Coin c1 = Coin(pt1_x,pt1_y,COLOR_BLACK,1);
                Coin c2 = Coin(pt2_x,pt2_y,COLOR_BLACK,1);
                temp.c1 = c1;
                temp.c2 = c2;
                temp.pt1_x = pt1_x;
                temp.pt1_y = pt1_y;
                temp.pt2_x = pt2_x;
                temp.pt2_y = pt2_y;
                enemies1.push_back(temp);
            }
            if(enemies2.size()<1 && t5a.processTick() && ball2.level>1) // Fire Beams 1 at a time and after 5 sec and after level 1
            {
                double pt1_x,pt1_y,pt2_x,pt2_y;
                pt1_x = rand() %575 +200;
                pt1_x =pt1_x/100;
                pt1_y = (rand() %300);
                pt1_y = pt1_y/100;
                pt2_x = pt1_x+3;
                while(abs(pt2_x-pt1_x)>2)
                {
                    pt2_x = (rand() %575 +200);
                    pt2_x = pt2_x/100;
                }
                pt2_y = pt1_y;
                double l = (pt2_x-pt1_x)/2;
                Enemy1 temp;
                if(pt1_x == pt2_x)
                    temp = Enemy1((pt1_x+pt2_x)/2,(pt2_y+pt1_y)/2,COLOR_RED,PI/2,l/cos(2*PI/180));
                else
                    temp = Enemy1((pt1_x+pt2_x)/2,(pt2_y+pt1_y)/2,COLOR_RED,atan((pt2_y - pt1_y)/(pt2_x - pt1_x)),l/cos(2*PI/180));
                
                Coin c1 = Coin(pt1_x,pt1_y,COLOR_BLACK,1);
                Coin c2 = Coin(pt2_x,pt2_y,COLOR_BLACK,1);
                temp.c1 = c1;
                temp.c2 = c2;
                temp.pt1_x = pt1_x;
                temp.pt1_y = pt1_y;
                temp.pt2_x = pt2_x;
                temp.pt2_y = pt2_y;
                enemies2.push_back(temp);
            }
            //Detecting Collisions 
            collision_coin();
            collision_ring();
            collision_enemy1();
            collision_boomerang();
            collision_water();
            collision_special_objects();

            glfwSwapBuffers(window);
            tick_elements();
            reset_screen();
            tick_input(window);

            vector<Coin>::iterator it;
            it = coins.begin();
            while(it!=coins.end())
            {
                (*it).tick(0);
                it++;
            }
            if(b1.exist)
                b1.tick();
            if(r1.exist)    
                r1.tick(0);
            it = ice_ball.begin();
            while(it!=ice_ball.end())
            {
                it->projectile();
                it++;
            }
            vector<Enemy1>::iterator eit;
            eit = enemies1.begin();
            while(eit!=enemies1.end())
            {
                (*eit).tick(0);
                (*eit).c1.tick(0);
                (*eit).c2.tick(0);
                eit++;
            }
            eit = enemies2.begin();
            while(eit!=enemies2.end())
            {
                (*eit).tick(3);
                (*eit).tick(0);
                (*eit).c1.tick(3);
                (*eit).c1.tick(0);
                (*eit).c2.tick(3);
                (*eit).c2.tick(0);
                eit++;
            }
            vector<Special_objects>::iterator sobjit;
            sobjit = sobj.begin();
            while(sobjit!=sobj.end())
            {
                if(!sobjit->exist && sobjit->flag == 0)
                {
                    ball2.tick(1);
                    if(t4.processTick())
                    {
                        ball2.x_acc =0 ;
                        ball2.x_speed = 0.015f;
                        sobj.erase(sobjit);
                        continue;
                    }
                }
                (*sobjit).tick();
                sobjit++;
            }

            vector<Platform>::iterator it1;
            it1 = platforms.begin();
            while(it1!=platforms.end())
            {
                (*it1).tick();               
                it1++;
            }
            if(platforms.size()<=6)
            {
                Platform temp;
                if(turn ==1)
                {
                    temp = Platform(pos,-7.5,COLOR_GREEN);
                    turn =0;
                }
                else
                {
                    turn =1;
                    temp = Platform(pos,-7.5,COLOR_BLACK);
                }
                platforms.push_back(temp);
                pos +=1.0f;
            }
            else
            {
                pos = 2.15f;
            }
            it1 = ceilings.begin();
            while(it1!=ceilings.end())
            {
                (*it1).tick();               
                it1++;
            }
            if(ceilings.size()<=6)
            {
                Platform temp;
                if(turn1 ==1)
                {
                    temp = Platform(pos1,7.5,COLOR_RED);
                    turn1 =0;
                }
                else
                {
                    turn1 =1;
                    temp = Platform(pos1,7.5,COLOR_GREEN);
                }
                ceilings.push_back(temp);
                pos1 +=1.0f;
            }
            else
            {
                pos1 = 2.15f;
            }
            vector<Water_ballon>::iterator w_it;
            w_it = wb.begin();
            while(w_it!=wb.end())
            {
                w_it->tick(1);
                w_it++;
            }
            w_it = fb.begin();
            while(w_it!=fb.end())
            {
                w_it->tick(1);
                w_it++;
            }
            prev_time = cur_time;
            draw();

        }

        // Poll for Keyboard and mouse events
        glfwPollEvents();
    }

    quit(window);
}

bool detect_collision(bounding_box_t a, bounding_box_t b) {
    return (abs(a.x - b.x) * 2 < (a.width + b.width)) &&
           (abs(a.y - b.y) * 2 < (a.height + b.height));
}

void reset_screen() {
    float top    = screen_center_y + 4 / screen_zoom;
    float bottom = screen_center_y - 4 / screen_zoom;
    float left   = screen_center_x - 4 / screen_zoom;
    float right  = screen_center_x + 4 / screen_zoom;
    Matrices.projection = glm::ortho(left, right, bottom, top, 0.1f, 500.0f);
}