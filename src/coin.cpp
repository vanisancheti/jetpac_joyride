#include "coin.h"
#include "main.h"
#include <iostream>
using namespace std;
Coin::Coin(float x, float y, color_t color,int r) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    x_speed = 0.05;
    y_speed = 0.05;
    gravity = 9.8;
    this->exist = true;
    this->type=r;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    
    // SIMPLE COINS
    int n1=20;    
    GLfloat vertex_buffer_data[n1*9];
    int temp=0;
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n1;
    double x0=0.0f,y0=0.25f,z0=0.0f;
    double x1=0.0f,y1=0.25f;
    for(int i=0;i<n1;i++)
    {
        vertex_buffer_data[temp++] = 0.0f;
        vertex_buffer_data[temp++] = 0.0f; 
        vertex_buffer_data[temp++] = 0.0f;
        vertex_buffer_data[temp++] = x0;
        vertex_buffer_data[temp++] = y0; 
        vertex_buffer_data[temp++] = z0; 
        x1 = x0*cos(arg)-y0*sin(arg);
        y1 = x0*sin(arg)+y0*cos(arg);
        x0 = x1;
        y0 = y1;
        vertex_buffer_data[temp++] = x0;
        vertex_buffer_data[temp++] = y0;
        vertex_buffer_data[temp++] = z0;
    }
    int n2=6;
    GLfloat vertex_buffer_data_1[n2*9];
    temp=0;
    arg = 2*PI/n2;
    x0=0.0f,y0=0.25f,z0=0.0f;
    for(int i=0;i<n2;i++)
    {
        vertex_buffer_data_1[temp++] = 0.0f;
        vertex_buffer_data_1[temp++] = 0.0f; 
        vertex_buffer_data_1[temp++] = 0.0f;
        vertex_buffer_data_1[temp++] = x0;
        vertex_buffer_data_1[temp++] = y0; 
        vertex_buffer_data_1[temp++] = z0; 
        x1 = x0*cos(arg)-y0*sin(arg);
        y1 = x0*sin(arg)+y0*cos(arg);
        x0 = x1;
        y0 = y1;
        vertex_buffer_data_1[temp++] = x0;
        vertex_buffer_data_1[temp++] = y0;
        vertex_buffer_data_1[temp++] = z0;
    }    

    if(r==0)
        this->object = create3DObject(GL_TRIANGLES, n2*3, vertex_buffer_data_1,COLOR_VIOLET, GL_FILL);
    else
        this->object = create3DObject(GL_TRIANGLES, n1*3, vertex_buffer_data,color, GL_FILL);
    
}

void Coin::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(0.1f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    glm::mat4 scale1 = glm::scale (scale);    
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (scale1 * translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
        draw3DObject(this->object);

}

void Coin::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Coin::projectile() {
    double inter = cur() - prev();
    double temp = (y_speed*inter - (gravity)*inter*inter/2);
    y_speed -= gravity*inter;

    this->position.x +=x_speed;
    if(this->position.y + temp >= -5.0f && this->position.y + temp <= 5.0f)
       this->position.y+=temp;
   else
    this->exist = false;
    if(this->position.x >= 20.0f || this->position.x <= -20.0f)
        this->exist = false;
}

void Coin::tick(int move) {
    if(move==1)
    {
        this->position.x += x_speed;
        if(this->position.x> 4.45f)
            this->position.x =0;
    }   
    if(move==0)
    {
        this->position.x -= x_speed;
        if(this->position.x< -14.45f)
            this->exist = false;
    }
    if(move ==2)
    {
        this->position.y += x_speed;
        if(this->position.y > 7.5f)
            this->exist = false;
    }
    if(move == 3)
    {
        this->position.y -=x_speed;
        if(this->position.y < -7.5f)
            this->exist = false;
    }
}


