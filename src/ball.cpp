#include "ball.h"
#include "main.h"
#include "object.h"
#include <iostream>
using namespace std;
Ball::Ball(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    this->score = 0;
    this->lives = 10;
    this->level = 1;
    this->ring = 0;
    this->sheild = 0;
    x_speed = 0.015;
    y_speed = 0.015;
    x_acc= 0;
    y_acceleration = 5;
    gravity = 9.8;
    this->border = 3.0f;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    int n=20;    
    GLfloat vertex_buffer_data[30*9];
    GLfloat vertex_buffer_data_2[2*9];
    GLfloat vertex_buffer_data_1[6*9];
    int temp=0;
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    double x0=0.25f,y0=0.0f,z0=0.0f;
    double x1=0.25f,y1=0.0f;
    for(int i=0;i<n;i++)
    {
        vertex_buffer_data[temp++] = 0.25f;
        vertex_buffer_data[temp++] = 0.25f; 
        vertex_buffer_data[temp++] = 0.0f;
        vertex_buffer_data[temp++] = x0+0.25f;
        vertex_buffer_data[temp++] = y0+0.25f; 
        vertex_buffer_data[temp++] = z0; 
        x1 = x0*cos(arg)-y0*sin(arg);
        y1 = x0*sin(arg)+y0*cos(arg);
        x0 = x1;
        y0 = y1;
        vertex_buffer_data[temp++] = x0+0.25f;
        vertex_buffer_data[temp++] = y0+0.25f;
        vertex_buffer_data[temp++] = z0;
    }

    temp=0;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = 0.5f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = -0.5f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = 0.5f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = -0.5f;
    vertex_buffer_data_2[temp++] = 0.0f;
    vertex_buffer_data_2[temp++] = 0.5f;
    vertex_buffer_data_2[temp++] = -0.5f;
    vertex_buffer_data_2[temp++] = 0.0f;
    temp=0;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.15f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.5f;
    vertex_buffer_data_1[temp++] = -0.15f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.35f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.5f;
    vertex_buffer_data_1[temp++] = -0.15f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.35f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.5f;
    vertex_buffer_data_1[temp++] = -0.35f;
    vertex_buffer_data_1[temp++] = 0.0f; 

    vertex_buffer_data_1[temp++] = -0.25f; 
    vertex_buffer_data_1[temp++] = 0.25f; 
    vertex_buffer_data_1[temp++] = 0.0f; 
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.15f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.5f;
    vertex_buffer_data_1[temp++] = -0.15f;
    vertex_buffer_data_1[temp++] = 0.0f;

    vertex_buffer_data_1[temp++] = -0.5f; 
    vertex_buffer_data_1[temp++] = -0.35f; 
    vertex_buffer_data_1[temp++] = 0.0f; 
    vertex_buffer_data_1[temp++] = -0.25f;
    vertex_buffer_data_1[temp++] = -0.35f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.35f;
    vertex_buffer_data_1[temp++] = -0.5f;
    vertex_buffer_data_1[temp++] = 0.0f;

    vertex_buffer_data_1[temp++] = 0.0f; 
    vertex_buffer_data_1[temp++] = -0.35f; 
    vertex_buffer_data_1[temp++] = 0.0f; 
    vertex_buffer_data_1[temp++] = -0.25f;
    vertex_buffer_data_1[temp++] = -0.35f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.15f;
    vertex_buffer_data_1[temp++] = -0.5f;
    vertex_buffer_data_1[temp++] = 0.0f;

    vertex_buffer_data_1[temp++] = -0.25f; 
    vertex_buffer_data_1[temp++] = -0.35f; 
    vertex_buffer_data_1[temp++] = 0.0f; 
    vertex_buffer_data_1[temp++] = -0.35f;
    vertex_buffer_data_1[temp++] = -0.5f;
    vertex_buffer_data_1[temp++] = 0.0f;
    vertex_buffer_data_1[temp++] = -0.15f;
    vertex_buffer_data_1[temp++] = -0.5f;
    vertex_buffer_data_1[temp++] = 0.0f;   
    this->object = create3DObject(GL_TRIANGLES, (n)*3, vertex_buffer_data, COLOR_BLUE, GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES, 8*3, vertex_buffer_data_1, COLOR_GREY, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data_2, COLOR_RED, GL_FILL);
}

void Ball::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(0.1f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    glm::mat4 scale1 = glm::scale (scale);    
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (scale1 * translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
    draw3DObject(this->object1);
    draw3DObject(this->object2);
}

void Ball::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Ball::tick(int move) {
    // this->rotation += x_speed;
    double inter = cur() - prev();
    if(move==1 || this->position.y > -2.9f)
    {
        double temp = x_speed*inter + (x_acc)*inter*inter/2;
        if(x_acc == 0)
            temp = x_speed;
        x_speed += x_acc*inter;        
        this->position.x += temp;
    }   
    if(move==0)
    {
        this->position.x -= x_speed;
        if(this->position.x< -4.45f)
            this->position.x +=x_speed;
    }
    if(move==2)
    {
        double temp = y_speed*inter + (y_acceleration)*inter*inter/2;
        y_speed += y_acceleration*inter;
        if(this->position.y + temp <= 3.0f && this->position.y + temp >= -3.0f)
           this->position.y +=temp;
        else
            y_speed=0;
    }
    if(move==3)
    {
        double temp = y_speed*inter - (gravity)*inter*inter/2;
        y_speed -= gravity*inter;
        // cout << temp  << " " << inter << endl;
        if(this->position.y + temp >= -this->border && this->position.y + temp <= this->border)
           this->position.y +=temp;
        else
            y_speed=0;    
    }
}


