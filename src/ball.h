#include "main.h"

#ifndef BALL_H
#define BALL_H


class Ball {
public:
    Ball() {}
    Ball(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    int score;
    int lives;
    int level;
    int sheild;
    int ring;
    double border;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y);
    void tick(int move);
    void collision(int flag);
    double x_speed;
    double y_speed;
    double x_acc;
    double y_acceleration;
    double gravity;
private:
    VAO *object;
    VAO *object1;
    VAO *object2;
};

#endif // BALL_H
