#include "main.h"

#ifndef WATER_BALLON_H
#define WATER_BALLON_H


class Water_ballon {
public:
    Water_ballon() {}
    Water_ballon(float x, float y, color_t color,int r);
    glm::vec3 position;
    float rotation;
    bool exist;
    int type;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y);
    void tick(int move);
    double x_speed;
    double y_speed;
    double gravity;
private:
    VAO *object;
    VAO *object1;

};

#endif // WATER_BALLON_H
