#include "ring.h"
#include "main.h"
#include <iostream>
using namespace std;
Ring::Ring(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    x_speed = 0.05;
    this->exist = true;
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    theta = PI;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    
    // SIMPLE COINS
    int n=26;    
    GLfloat vertex_buffer_data[n*9];
    GLfloat vertex_buffer_data_1[n*9];
    int temp=0;
    double arg = -2*PI/n;
    double x0=2.0f,y0=0.0f,z0=0.0f;
    double x1=0.0f,y1=2.0f;
    for(int i=0;i<n/2;i++)
    {
        vertex_buffer_data[temp++] = 0.0f;
        vertex_buffer_data[temp++] = 0.0f; 
        vertex_buffer_data[temp++] = 0.0f;
        vertex_buffer_data[temp++] = x0;
        vertex_buffer_data[temp++] = y0; 
        vertex_buffer_data[temp++] = z0; 
        x1 = x0*cos(arg)-y0*sin(arg);
        y1 = x0*sin(arg)+y0*cos(arg);
        x0 = x1;
        y0 = y1;
        vertex_buffer_data[temp++] = x0;
        vertex_buffer_data[temp++] = y0;
        vertex_buffer_data[temp++] = z0;
    }
    temp=0;
    x0=1.5f,y0=0.0f,z0=0.0f;
    x1=0.0f,y1=2.0f;
    for(int i=0;i<n/2;i++)
    {
        vertex_buffer_data_1[temp++] = 0.0f;
        vertex_buffer_data_1[temp++] = 0.0f; 
        vertex_buffer_data_1[temp++] = 0.0f;
        vertex_buffer_data_1[temp++] = x0;
        vertex_buffer_data_1[temp++] = y0; 
        vertex_buffer_data_1[temp++] = z0; 
        x1 = x0*cos(arg)-y0*sin(arg);
        y1 = x0*sin(arg)+y0*cos(arg);
        x0 = x1;
        y0 = y1;
        vertex_buffer_data_1[temp++] = x0;
        vertex_buffer_data_1[temp++] = y0;
        vertex_buffer_data_1[temp++] = z0;
    }

    this->object = create3DObject(GL_TRIANGLES, (n/2)*3, vertex_buffer_data, color, GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES, (n/2)*3, vertex_buffer_data_1, COLOR_WALL, GL_FILL);
    
}

void Ring::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(0.1f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    glm::mat4 scale1 = glm::scale (scale);    
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (scale1 * translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
    draw3DObject(this->object1);

}

void Ring::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Ring::tick(int move) {
    if(move==1)
    {
        this->position.x += x_speed;
        if(this->position.x> 4.45f)
            this->position.x =0;
    }   
    if(move==0)
    {
        this->position.x -= x_speed;
        if(this->position.x< -4.45f)
            this->exist = false;
    }
}


