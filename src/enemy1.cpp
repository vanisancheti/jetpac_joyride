#include "enemy1.h"
#include "main.h"
#include <iostream>
using namespace std;
Enemy1::Enemy1(float x, float y, color_t color,double arg,double size) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    x_speed = 0.05;
    y_speed = 0.05;
    this->exist = true;
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    GLfloat vertex_buffer_data[] = {
        size*cos(arg+2*(PI/180)),size*sin(arg+2*(PI/180)),0.0f,
        size*cos(arg-2*(PI/180)),size*sin(arg-2*(PI/180)),0.0f,
        -size*cos(arg-2*(PI/180)),-size*sin(arg-2*(PI/180)),0.0f,
        size*cos(arg-2*(PI/180)),size*sin(arg-2*(PI/180)),0.0f,
        -size*cos(arg-2*(PI/180)),-size*sin(arg-2*(PI/180)),0.0f,
        -size*cos(arg+2*(PI/180)),-size*sin(arg+2*(PI/180)),0.0f
    };
    this->object = create3DObject(GL_TRIANGLES,2*3, vertex_buffer_data, color, GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES,2*3, vertex_buffer_data, COLOR_ORANGE, GL_FILL);
}

void Enemy1::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(0.1f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation), glm::vec3(1, 0, 0));
    glm::mat4 scale1 = glm::scale (scale);    
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (scale1 * translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    // draw3DObject(this->object);
    int x = rand()%4;
    if(x==0)
        draw3DObject(this->object1);
    else
        draw3DObject(this->object);
}

void Enemy1::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Enemy1::tick(int move) {
    // this->rotation += x_speed;
    double inter = cur() - prev();
    if(move==1)
    {
        this->position.x += x_speed;
        this->pt1_x += x_speed;
        this->pt2_x += x_speed;
        if(this->position.x> 5.45f)
            this->exist = false;
    }   
    if(move==0)
    {
        this->position.x -= x_speed;
        this->pt1_x -= x_speed;
        this->pt2_x -= x_speed;
        if(this->position.x< -5.45f)
            this->exist = false;
    }
   if(move == 2)
   {
        this->position.y += y_speed;
        this->pt1_y += y_speed;
        this->pt2_y += y_speed;
        if(this->position.y > 3.0f)
            this->exist = false;
   }
   if(move == 3)
   {
        this->position.y -= y_speed;
        this->pt1_y -= y_speed;
        this->pt2_y -= y_speed;
        if(this->position.y < -3.0f)
            this->exist = false;
   }
}


