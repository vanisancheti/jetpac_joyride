#include "boomerang.h"
#include "main.h"
#include <iostream>
using namespace std;
Boomerang::Boomerang(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    this->turn = 0;
    x_speed = 0.15;
    y_speed = 0;
    this->exist = true;
    GLfloat vertex_buffer_data[] = {
        0.0f,0.0f ,0.0f,
        0.25f,0.0f,0.0f,
        0.5f,-0.5f,0.0f,
        0.0f,0.0f ,0.0f,
        0.25f,0.0f,0.0f,
        0.5f,0.5f,0.0f,
    };
    GLfloat vertex_buffer_data1[] = {
        0.0f,0.0f ,0.0f,
        -0.25f,0.0f,0.0f,
        -0.5f,-0.5f,0.0f,
        0.0f,0.0f ,0.0f,
        -0.25f,0.0f,0.0f,
        -0.5f,0.5f,0.0f,
    };
    this->object = create3DObject(GL_TRIANGLES,2*3, vertex_buffer_data, color, GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES,2*3, vertex_buffer_data1, color, GL_FILL);
}

void Boomerang::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(0.1f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation), glm::vec3(1, 0, 0));
    glm::mat4 scale1 = glm::scale (scale);    
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (scale1 * translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    if(this->turn == 0)
        draw3DObject(this->object);
    else
        draw3DObject(this->object1);
}

void Boomerang::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Boomerang::tick() {
    // this->rotation += x_speed;
    double k = abs((this->position.x+4)/(this->position.y*this->position.y));

    // cout << this->position.x << " " << this->position.y  << " "  << k << endl;
    if(this->position.x > -4 && this->position.y>0)
    {
        this->position.x -= x_speed;
        if(this->position.x < -4)
            this->position.y = -sqrt(-(this->position.x+4)/k);
        else
            this->position.y = sqrt((this->position.x+4)/k);
    }    
    else
        if(this->position.x <= 4)
        {
            this->turn = 1;
            this->position.x += x_speed;
            if(this->position.x < -4)
                this->position.y = -sqrt(-(this->position.x+4)/k);
            else
                this->position.y = -sqrt((this->position.x+4)/k);
        }
        else
        {
            this->exist = false;
        }
}


