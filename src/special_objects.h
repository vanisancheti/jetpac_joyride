#include "main.h"
#ifndef SPECIAL_OBJECTS_H
#define SPECIAL_OBJECTS_H


class Special_objects {
public:
    Special_objects() {}
    Special_objects(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    bool exist;
    int flag;
    double gravity;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y);
    void tick();
    void collision(int flag);
    double x_speed;
    double y_speed;
private:
    VAO *object_m;
    VAO *object_h;
    VAO *object_s;
};

#endif // SPECIAL_OBJECTS_H
