#include "main.h"
#include "coin.h"
#ifndef DRAGON_H
#define DRAGON_H


class Dragon {
public:
    Dragon() {}
    Dragon(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    bool exist;
    Coin c1;
    int type;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y);
    void tick(int move);
    double x_speed;
private:
    VAO *object;
    VAO *object1;
    VAO *object2;
};

#endif // DRAGON_H
