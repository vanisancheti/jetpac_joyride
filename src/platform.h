#include "main.h"

#ifndef PLATFORM_H
#define PLATFORM_H


class Platform {
public:
    Platform() {}
    Platform(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    bool exist;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y);
    void tick();
    double x_speed;
private:
    VAO *object;
    VAO *object1;
};

#endif // PLATFORM_H
