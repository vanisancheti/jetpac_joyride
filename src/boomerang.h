#include "main.h"
#include "coin.h"
#ifndef BOOMERANG_H
#define BOOMERANG_H


class Boomerang{
public:
    Boomerang() {}
    Boomerang(float x, float y, color_t color);
    glm::vec3 position;
    float rotation;
    int turn;
    bool exist;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y);
    void tick();
    void collision(int flag);
    double x_speed;
    double y_speed;
private:
    VAO *object;
    VAO *object1;
};

#endif // BOOMERANG_H
