#include "dragon.h"
#include "main.h"
#include <iostream>
using namespace std;
Dragon::Dragon(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    x_speed = 0.015;
    this->exist = true;
    this->type=0;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    
    // SIMPLE COINS
    GLfloat vertex_buffer_data [] = {
        0.25f,0.25f,0.0f,
        -0.25f,-0.25f,0.0f,
        0.25f,-0.25f,0.0f,
        -0.25f,-0.25f,0.0f,
        0.25f,0.25f,0.0f,
        -0.25f,0.25f,0.0f
    };
    GLfloat vertex_buffer_data_1 [] = {
        -0.25f,0.25f,0.0f,
        0.25f,0.25f,0.0f,
        0.5f,0.5f,0.0f,
        -0.25f,-0.25f,0.0f,
        0.25f,-0.25f,0.0f,
        0.5f,-0.5f,0.0f,
        -0.25f,0.25f,0.0f,
        -0.5f,0.0f,0.0f,
        -0.25f,-0.25f,0.0f
    };
    GLfloat vertex_buffer_data_2 [] = {
        -0.25f,0.25f,0.0f,
        0.25f,0.25f,0.0f,
        -0.5f,0.5f,0.0f,
        -0.25f,-0.25f,0.0f,
        0.25f,-0.25f,0.0f,
        -0.5f,-0.5f,0.0f,
        0.25f,0.25f,0.0f,
        0.5f,0.0f,0.0f,
        0.25f,-0.25f,0.0f
    };
    this->object = create3DObject(GL_TRIANGLES, 2*3, vertex_buffer_data,COLOR_GREY, GL_FILL);
    this->object1 = create3DObject(GL_TRIANGLES, 3*3, vertex_buffer_data_1,COLOR_RED, GL_FILL);
    this->object2 = create3DObject(GL_TRIANGLES, 3*3, vertex_buffer_data_2,COLOR_RED, GL_FILL);
    
}

void Dragon::draw(glm::mat4 VP,glm::vec3 scale) {
    Matrices.model = glm::mat4(0.1f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    glm::mat4 scale1 = glm::scale (scale);    
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (scale1 * translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
        draw3DObject(this->object);
    if(this->type == 0)
        draw3DObject(this->object1);
    else
        draw3DObject(this->object2);

}

void Dragon::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Dragon::tick(int move) {
    if(move==1)
    {
        this->position.x += x_speed;
        if(this->position.x> 4.45f)
            this->position.x =0;
    }   
    if(move==0)
    {
        this->position.x -= x_speed;
        if(this->position.x< -14.45f)
            this->exist = false;
    }
    if(move ==2)
    {
        this->position.y += x_speed;
        if(this->position.y > 7.5f)
            this->exist = false;
    }
    if(move == 3)
    {
        this->position.y -=x_speed;
        if(this->position.y < -7.5f)
            this->exist = false;
    }
}


