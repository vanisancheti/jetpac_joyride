#include "main.h"

const color_t COLOR_RED = { 236, 100, 75 ,0};
const color_t COLOR_GREEN = { 135, 211, 124 ,0};
const color_t COLOR_BLUE = { 52, 73, 94 ,0};
const color_t COLOR_LIGHTBLUE = { 0, 191, 255 ,0};
const color_t COLOR_BLACK = { 0, 0, 0 ,0};
const color_t COLOR_GOLD = { 249, 231, 71,0 };
const color_t COLOR_GREY = { 96, 96, 96,0 };
const color_t COLOR_WGREY = { 153, 255, 255,0 };
const color_t COLOR_WALL = { 192, 192, 192,0 };
const color_t COLOR_VIOLET = { 51, 0, 102,0 };
const color_t COLOR_ORANGE = { 252, 153, 67,0 };
// const color_t COLOR_BACKGROUND = { 52, 73, 94 };
const color_t COLOR_BACKGROUND = { 242, 241, 239 ,0};
