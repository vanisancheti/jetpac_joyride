#include "sphere.h"
#include "main.h"
#include <iostream>

Sphere::Sphere(float x, float y, color_t color) {
    this->position = glm::vec3(x, y, 0);
    this->rotation = 0;
    speed = 0.5;
    // Our vertices. Three consecutive floats give a 3D vertex; Three consecutive vertices give a triangle.
    // A cube has 6 faces with 2 triangles each, so this makes 6*2=12 triangles, and 12*3 vertices
    int n=100;    
    GLfloat vertex_buffer_data[n*9*n];
    int temp=0;
    double PI = 3.141592653589793238462643383279502884197169399375105820974944;
    double arg = 2*PI/n;
    double theta = 0;
    double x0=0.0f,y0=0.5f,z0=0.0f;
    double x1=0.0f,y1=0.5f,z1=0.0f;
    for(int j=0;j<n;j++)
    {
        z0 = 0.5*sin(theta);
        for(int i=0;i<n;i++)
        {
            vertex_buffer_data[temp++] = 0.0f;
            vertex_buffer_data[temp++] = 0.0f; 
            vertex_buffer_data[temp++] = 0.0f;
            vertex_buffer_data[temp++] = x0;
            vertex_buffer_data[temp++] = y0; 
            vertex_buffer_data[temp++] = z0; 
            x1 = x0*cos(arg)-y0*sin(arg);
            y1 = x0*sin(arg)+y0*cos(arg);
            x0 = x1;
            y0 = y1;
            z0 = x0*sin(theta);
            std::cout << z0 << std::endl;
            vertex_buffer_data[temp++] = x0;
            vertex_buffer_data[temp++] = y0;
            vertex_buffer_data[temp++] = z0;
        }   
        theta += 15*PI; 
        // break;
    }
    // for(int i=0;i<n;i++)
    // {
    //     vertex_buffer_data[temp++] = 0.0f;
    //     vertex_buffer_data[temp++] = 0.0f; 
    //     vertex_buffer_data[temp++] = 0.0f;
    //     vertex_buffer_data[temp++] = x0;
    //     vertex_buffer_data[temp++] = y0; 
    //     vertex_buffer_data[temp++] = z0; 
    //     x1 = x0*cos(arg)-y0*sin(arg);
    //     y1 = x0*sin(arg)+y0*cos(arg);
    //     x0 = x1;
    //     y0 = y1;
    //     vertex_buffer_data[temp++] = x0;
    //     vertex_buffer_data[temp++] = y0;
    //     vertex_buffer_data[temp++] = z0;
    // }
    // x0=0.0f,y0=0.5f,z0=0.0f;
    // x1=0.0f,y1=0.5f,z1=0.0f;
    // for(int i=0;i<n;i++)
    // {
    //     vertex_buffer_data[temp++] = 0.0f;
    //     vertex_buffer_data[temp++] = 0.0f; 
    //     vertex_buffer_data[temp++] = 0.0f;
    //     vertex_buffer_data[temp++] = x0;
    //     vertex_buffer_data[temp++] = y0; 
    //     vertex_buffer_data[temp++] = z0; 
    //     z1 = z0*cos(arg)-y0*sin(arg);
    //     y1 = z0*sin(arg)+y0*cos(arg);
    //     z0 = z1;
    //     y0 = y1;
    //     vertex_buffer_data[temp++] = x0;
    //     vertex_buffer_data[temp++] = y0;
    //     vertex_buffer_data[temp++] = z0;
    // }
    // x0=0.0f,y0=0.5f,z0=0.0f;
    // x1=0.0f,y1=0.5f,z1=0.0f;
    // for(int i=0;i<n;i++)
    // {
    //     vertex_buffer_data[temp++] = 0.0f;
    //     vertex_buffer_data[temp++] = 0.0f; 
    //     vertex_buffer_data[temp++] = 0.0f;
    //     vertex_buffer_data[temp++] = x0;
    //     vertex_buffer_data[temp++] = y0; 
    //     vertex_buffer_data[temp++] = z0; 
    //     z1 = z0*cos(arg)-x0*sin(arg);
    //     x1 = z0*sin(arg)+x0*cos(arg);
    //     z0 = z1;
    //     x0 = x1;
    //     vertex_buffer_data[temp++] = x0;
    //     vertex_buffer_data[temp++] = y0;
    //     vertex_buffer_data[temp++] = z0;
    // }

    this->object = create3DObject(GL_TRIANGLES, n*3*n, vertex_buffer_data, color, GL_FILL);
}

void Sphere::draw(glm::mat4 VP) {
    Matrices.model = glm::mat4(0.1f);
    glm::mat4 translate = glm::translate (this->position);    // glTranslatef
    glm::mat4 rotate    = glm::rotate((float) (this->rotation * M_PI / 180.0f), glm::vec3(1, 0, 0));
    // No need as coords centered at 0, 0, 0 of cube arouund which we waant to rotate
    // rotate          = rotate * glm::translate(glm::vec3(0, -0.6, 0));
    Matrices.model *= (translate * rotate);
    glm::mat4 MVP = VP * Matrices.model;
    glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
    draw3DObject(this->object);
}

void Sphere::set_position(float x, float y) {
    this->position = glm::vec3(x, y, 0);
}

void Sphere::tick(int move) {
    this->rotation += speed;
    // if(move==1)
    // this->position.x += speed;
    // else   
    // this->position.x -= speed;
}
// void Ball::right() {
//     this->position.x += speed;
// }
// void Ball::left() {
//     this->position.x -= speed;
// }

