#include "main.h"

#ifndef COIN_H
#define COIN_H


class Coin {
public:
    Coin() {}
    Coin(float x, float y, color_t color,int r);
    glm::vec3 position;
    float rotation;
    bool exist;
    int type;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y);
    void tick(int move);
    void projectile();
    double x_speed;
    double y_speed;
    double gravity;
private:
    VAO *object;

};

#endif // COIN_H
