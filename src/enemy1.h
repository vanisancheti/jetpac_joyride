#include "main.h"
#include "coin.h"
#ifndef ENEMY1_H
#define ENEMY1_H


class Enemy1 {
public:
    Enemy1() {}
    Enemy1(float x, float y, color_t color,double arg,double size);
    glm::vec3 position;
    float rotation;
    Coin c1;
    Coin c2;
    bool exist;
    double pt1_x,pt1_y,pt2_x,pt2_y;
    void draw(glm::mat4 VP,glm::vec3 scale);
    void set_position(float x, float y);
    void tick(int move);
    void collision(int flag);
    double x_speed;
    double y_speed;
private:
    VAO *object;
    VAO *object1;
};

#endif // ENEMY1_H
